# Merge Request pipelines

Ensure pipeline runs for merge requests.

This project contains a single default component that allows to run pipelines:
- for changes to the `main` branch.
- when a tag is created.
- for changes to open merge requests.

## Usage

```yaml
include:
  - component: gitlab.com/gitlab-test-ci-catalog/catalog/merge-request-pipelines@1.0
```
